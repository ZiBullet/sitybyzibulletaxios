function friendsModalOpen(modal, modal__bg) {
    modal.style.display = 'flex'
    modal__bg.style.display = 'block'
    setTimeout(() => {
        modal__bg.style.opacity = '1'
        modal.style.left = '0'
    }, 300)
}
function friendModalClose(modal, modal__bg) {
    modal__bg.style.opacity = '0'
    modal.style.left = '-100%'
    setTimeout(() => {
        modal.style.display = 'none'
        modal__bg.style.display = 'none'
    }, 300)
}
let friendsContainer = document.querySelector('.friends__modal')
function reloadFriends (friends) {
    friendsContainer.innerHTML = ''
    for (let friend of friends) {
        let friendProfile = document.createElement('div')
        let friendAvatar = document.createElement('img')
        let friendName = document.createElement('h4')
        let friendAge = document.createElement('span')
        
        friendProfile.classList.add('friend-profile')
        friendAvatar.src = friend.photo.name
        friendName.innerHTML = friend.name
        friendAge.innerHTML = friend.age

        friendsContainer.append(friendProfile)
        friendProfile.append(friendAvatar, friendName, friendAge)
    }
}
function profileModalOpen (modal, modal__bg) {
    modal.style.display = 'flex'
    modal__bg.style.display = 'block'
    setTimeout(() => {
        modal__bg.style.opacity = '1'
        modal.style.right = '0'
    }, 300)
}
function reloadProfile(information) {
    let cont = document.querySelector('.profile__modal')
    let userAvatar = cont.querySelector('img')
    let userName = cont.querySelector('h4')
    let userAge = cont.querySelector('span')
    let insta = cont.querySelector('.insta')
    let telegram = cont.querySelector('.telegram')
    let userInfo = cont.querySelector('p')
    console.log(userAvatar);
    for (let info of information) {
        userName.innerHTML = info.name
        userAge.innerHTML = info.age
        insta.href = info.insta
        telegram.href = info.telegram
        userInfo.innerHTML = info.description
        userAvatar.src = info.avatar
    }
}
function profileModalClose (modal, modal__bg) {
    modal__bg.style.opacity = '0'
    modal.style.right = '-100%'
    setTimeout(() => {
        modal.style.display = 'none'
        modal__bg.style.display = 'none'
    }, 300)
}
export {friendsModalOpen, friendModalClose, reloadFriends, profileModalOpen, profileModalClose, reloadProfile}

export function adderOpen (modal) {
    modal.style.display = 'flex'
    setTimeout(() => {
        modal.style.transform = 'translateX(0)'
    }, 300)
}
export function adderClose (modal) {
    modal.style.transform = 'translateX(-120%)'
    setTimeout(() => {
        modal.style.display = 'none'
    }, 300)
}
export function profileEditorOpen (modal) {
    modal.style.display = 'flex'
    setTimeout(() => {
        modal.style.transform = 'translateX(0)'
    }, 300)
}
export function profileEditorClose (modal) {
    modal.style.transform = 'translateX(120%)'
    setTimeout(() => {
        modal.style.display = 'none'
    }, 300)
}

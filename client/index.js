// _______________________________Import area__________________________________
import {friendsModalOpen, friendModalClose, reloadFriends, profileModalOpen, profileModalClose, reloadProfile} from './scripts/modals.js'
import {adderOpen, adderClose, profileEditorOpen, profileEditorClose} from './scripts/modals.js'
// _____________________________________________________________________________
let url = 'http://localhost:3001'
function getAxios(direction, func) {
    axios.get(`${url}/${direction}`)
        .then(res => {
            if (res.status === 200 || res.status === 201) {
                func(res.data)
            }
        })
}
function postAxios(direction, item, func) {
    axios.post(`${url}/${direction}`, item)
        .then(res => {
            if (res.status === 200 || res.status === 201) {
                func(res.data)
            }
        })
}
let friendModal = document.querySelector('.friends__modal-canvas')
let friendModalBg = document.querySelector('.friends__modal-bg')
const showAllFriends = document.querySelector('.show__friends')
showAllFriends.onclick = () => {
    friendsModalOpen(friendModal, friendModalBg)
    getAxios('friends', reloadFriends)
}
let forms = document.forms
function avasBtn (avatars) {
    if (avatars.length >= 2) {
        let sliced = avatars.slice(0, 2)
        for (let avatar of sliced) {
            let ava = document.createElement('img')
            ava.src = avatar.photo.name
            showAllFriends.append(ava)
        }
    } else {
        for (let avatar of avatars) {
            let ava = document.createElement('img')
            ava.src = avatar.photo
            showAllFriends.append(ava)
        }
    }
}
getAxios('friends', avasBtn)
const friendModalCloserBtns = document.querySelectorAll('.friends__modal-closer')
friendModalCloserBtns.forEach(btn => {
    btn.onclick = () => {
        friendModalClose(friendModal, friendModalBg)
    }
})
let showProfile = document.querySelector('.show__profile')
let profileModal = document.querySelector('.profile__modal')
let profileModalBg = document.querySelector('.profile__modal-bg')
showProfile.onclick = () => {
    profileModalOpen(profileModal, profileModalBg)
    getAxios('personal_info', reloadProfile)
}
const profileModalClosers = document.querySelectorAll('.profile__modal-closer')
profileModalClosers.forEach(closer => {
    closer.onclick = () => {
        profileModalClose(profileModal, profileModalBg)
    }
})
const adderOpener = document.querySelector('.heading__adder')
const friendAdderModal = document.querySelector('.friend__adder__modal') 
const friendAdderCloser = document.querySelector('.modal__adder-closer')
adderOpener.onclick = () => {
    adderOpen(friendAdderModal)
}
friendAdderCloser.onclick = () => {
    adderClose(friendAdderModal)
}
for (let form of forms) {
    form.onsubmit = (event) => {
        event.preventDefault()
        submit(form)
    } 
}
function submit (form) {
    let user = {
        "id": Math.random()
    }
    let fm = new FormData(form)
    fm.forEach((value, key) => {
        user[key] = value
    })
    postAxios('friends', user, reloadFriends)
}
const editorCloserBtn = document.querySelector('.editor__modal-closer')
const profileEditorModal = document.querySelector('.profile__editor-modal')
const profileEditorBtn = document.querySelector('.heading__editor')
profileEditorBtn.onclick = () => {
    profileEditorOpen(profileEditorModal)
}
editorCloserBtn.onclick = () => {
    profileEditorClose(profileEditorModal)
}